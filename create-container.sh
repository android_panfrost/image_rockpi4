#!/bin/bash

set -e
set -o xtrace

export DEBIAN_FRONTEND=noninteractive

apt-get install -y ca-certificates

echo 'deb http://ftp.debian.org/debian unstable main' > /etc/apt/sources.list.d/experimental.list

apt-get update

apt-get dist-upgrade -y

apt-get install -y --no-remove \
                bison \
                bmap-tools \
                build-essential \
                ccache \
                debos \
                curl \
                flex \
                gcc-multilib \
                gettext \
                git-core \
                g++-multilib \
                gnupg \
                gperf \
                kpartx \
                lib32ncurses5-dev \
                lib32z-dev \
                libc6-dev-i386 \
                libgl1-mesa-dev \
                libncurses5 \
                libx11-dev \
                libxml2-utils \
                procps \
                python \
                python-pip \
                python-setuptools \
                simg2img \
                rsync \
                unzip \
                x11proto-core-dev \
                xsltproc \
                xz-utils \
                zip \
                zlib1g-dev


mkdir -p /usr/share/man/man1
apt-get install -y -t unstable openjdk-8-jdk

# Install docker
apt-get install -y apt-transport-https ca-certificates curl software-properties-common gnupg2
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable"
apt-get update
apt-get install -y docker-ce

curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo
chmod a+x /usr/local/bin/repo

git config --global user.name 'Android Build'
git config --global user.email 'foo@example.com'
git config --global color.ui true

mkdir -p /ccache
export USE_CCACHE=1
export CCACHE_DIR=/ccache

mkdir -p /aosp
pushd /aosp
repo init --depth=1 -u https://android.googlesource.com/platform/manifest -b ${CI_COMMIT_REF_NAME}

pushd .repo
git clone https://gitlab.collabora.com/android_panfrost/local_manifests.git local_manifests -b ${CI_COMMIT_REF_NAME}
popd

repo sync -q --force-sync --no-clone-bundle --no-tags -j30
du -sh /aosp

# Update to the latest repo vesion available
cp /aosp/.repo/repo/repo /usr/local/bin/repo
chmod a+x /usr/local/bin/repo

# python-mako from the host is not used, but is still needed
# so let's install it into the Android python search path
pip install --target=/aosp/development/python-packages mako --ignore-installed

export USER=$(whoami)

# Sanity check, verify that we can select _some_ target
source build/envsetup.sh
lunch aosp_arm64-eng

du -sh /aosp

popd

